
const db=require('../db')
const utils=require('../utils')
const express=require('express')
const router=express.Router()



router.get('/',(request,response)=>{
const query='select id,name,date,time,dir from movie'
db.pool.execute(query,(error,movies)=>{
response.send(utils.createResult(error,movies))
})
})

router.post('/',(request,response)=>{
    const {id,nm,dt,tm,dr}=request.body
    const query='insert into movie(id,name,date,time,dir) values(?,?,?,?)'
    db.pool.execute(query,[id,nm,dt,tm,dr],(error,result)=>{
        response.send(utils.createResult(error,result))
    })

})


router.delete('/',(request,response)=>{
    const {id}=request.body
    const query='delete from movie where id=?'
    db.pool.execute(query,[id],(error,result)=>{
        console.log(error);
        response.send(utils.createResult(error,result))
    })

})



module.exports=router