const express=require('express')
const mysql=require("mysql2")
const cors=require('cors');
const routermovie=require('./routes/movie')
const app=express()
app.use(cors('*'))
app.use(express.json())
app.use('/movie',routermovie)
app.listen('4000','0.0.0.0',()=>{
    console.log("Connected to server on port No 4000");
})